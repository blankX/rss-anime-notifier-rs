# RSS Anime Notifier but in Rust

Get anime schedule from https://anichart.net and gives an RSS feed to episodes that have already aired to standard output  

### How to Build and Run
https://doc.rust-lang.org/stable/book/ch01-03-hello-cargo.html#building-and-running-a-cargo-project

### How to Use
`rss-anime-notifier-rs <anime id>`  
For example, if the URL is https://anilist.co/anime/112609/Majo-no-Tabitabi, the anime ID is 112609  
To use with newsboat, check out https://brokenco.de/2020/07/07/newsboat-wacky-feeds.html
